This amazing tool allows you to rapidly clone modsec(ModSecurity) rule whitelists from one reseller to all the reseller's cPanel subaccounts. THis is helpful when a reseller is constantly tripping rules across multiple accounts and getting themselves blocked cause not all domains and/or cPanel accounts have the same whitelisted rules.

How to use:

Login to WHM:

Then navigate to:
Home » Plugins » ConfigServer ModSecurity Control 

Select the reseller user and click the "Modify user whitelist"

Then add all the desired rules to the reseller and save to apply.

As root then run the below and replace "reseller" with the username of the reseller in cPanel.
```
link='https://gitlab.com/cpaneltoolsnscripts/bulk-modsecwhitelist/-/raw/master/bulk_modsec_whitelist.sh'; bash <(curl -s $link || wget -qO - $link) reseller
```

See it in action below. This is from a real run the usernames have been switched with generic placeholders.

```
root@server [/root]# link='https://gitlab.com/cpaneltoolsnscripts/bulk-modsecwhitelist/-/raw/master/bulk_modsec_whitelist.sh'; bash <(curl -s $link || wget -qO - $link) reseller
Built /etc/apache2/conf/httpd.conf OK
/usr/local/apache/conf/userdata/ssl/2_4/

Find reseller cPanel User's accounts

username1
/usr/local/apache/conf/userdata/ssl/2_4/username1/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username2
/usr/local/apache/conf/userdata/ssl/2_4/username2/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username3
/usr/local/apache/conf/userdata/ssl/2_4/username3/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username4
/usr/local/apache/conf/userdata/ssl/2_4/username4/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username5
/usr/local/apache/conf/userdata/ssl/2_4/username5/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username6
/usr/local/apache/conf/userdata/ssl/2_4/username6/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username7
/usr/local/apache/conf/userdata/ssl/2_4/username7/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
username8
/usr/local/apache/conf/userdata/ssl/2_4/username8/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
reseller
mkdir: cannot create directory ‘/usr/local/apache/conf/userdata/ssl/2_4/reseller/’: File exists
cp: ‘/usr/local/apache/conf/userdata/ssl/2_4/reseller/modsec.conf’ and ‘/usr/local/apache/conf/userdata/ssl/2_4/reseller/modsec.conf’ are the same file
/usr/local/apache/conf/userdata/ssl/2_4/reseller/modsec.conf has been created
# Do not modify this file directly as it will be overwritten by cmc
<IfModule mod_security2.c>
SecRuleRemoveById 5004
SecRuleRemoveById 5000900
SecRuleRemoveById 60101
<LocationMatch .*>
        SecRuleRemoveById 5004
        SecRuleRemoveById 5000900
        SecRuleRemoveById 60101
</LocationMatch>
</IfModule>
All Modsec rules have been copied from reseller to the input accounts
root@server [/root]#
```
