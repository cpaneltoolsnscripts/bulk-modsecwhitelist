#!/bin/bash
## Author: Michael Ramsey
##Bulk modsec rule copying to a batch of cPanel accounts from one user
## For use on cPanel/WHM servers using ConfigServer Modsecurity plugin https://www.configserver.com/cp/cmc.html
## How to use: Run with the cpanel username you want to copy the rule from. It will copy that persons ModSecurity rules to all cPanel account usernames listed in the file "accounts.txt" 
#nano bulk_modsec_whitelist.sh username
# bash bulk_modsec_whitelist.sh username

Reseller=$1

#Ensure "userdata/ssl/2_4/" data structure is created Source:https://forums.cpanel.net/threads/add-line-of-code-to-vhost.598267/
/scripts/ensure_vhost_includes --all-users


#Find Apache vHost directory structure to add rule
if [ -d "/etc/apache2/conf.d/userdata/ssl/2_4/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_4/ exists"
   CurrentApacheVhostPathSSL='/etc/apache2/conf.d/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/etc/apache2/conf.d/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2_4/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_4/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2_2/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2_2/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2_2/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2_2/'
   echo "$CurrentApacheVhostPathSSL"
elif [ -d "/usr/local/apache/conf/userdata/ssl/2/" ]; then
   #echo "Folder /usr/local/apache/conf/userdata/ssl/2/ exists"
   CurrentApacheVhostPathSSL='/usr/local/apache/conf/userdata/ssl/2/'
   CurrentApacheVhostPathSTD='/usr/local/apache/conf/userdata/std/2/'
   echo "$CurrentApacheVhostPathSSL"
else
   echo "Unable to detect current CurrentApacheVhostPath for this server"
   echo "Creating folder structure"
   CurrentApacheVhostPathSSL='/etc/apache2/conf.d/userdata/ssl/2_4/'
   CurrentApacheVhostPathSTD='/etc/apache2/conf.d/userdata/std/2_4/'
   echo "$CurrentApacheVhostPathSSL"
   mkdir -p "$CurrentApacheVhostPathSSL"
   mkdir -p "$CurrentApacheVhostPathSTD"
fi


readarray -t reseller_acct_array < <(sudo grep $Reseller /etc/trueuserowners|awk -F":" '{print $1}'| tr '/' '\n');echo ""; echo "Find $Reseller cPanel User's accounts"; echo ""; 

for ACCT in "${reseller_acct_array[@]}"; do echo $ACCT; 

mkdir -p "$CurrentApacheVhostPathSSL""$ACCT"/
chmod 0755 "$CurrentApacheVhostPathSSL""$ACCT"/
cp "$CurrentApacheVhostPathSSL""$1"/modsec.conf "$CurrentApacheVhostPathSSL""$ACCT"/modsec.conf
chmod 0644 "$CurrentApacheVhostPathSSL""$ACCT"/modsec.conf
echo "$CurrentApacheVhostPathSSL${ACCT}/modsec.conf has been created"
cat $CurrentApacheVhostPathSSL"$ACCT"/modsec.conf
echo ""
done;
echo "All Modsec rules have been copied from $1 to the input accounts"